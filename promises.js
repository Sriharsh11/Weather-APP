const yargs = require('yargs');
const axios = require('axios');
const argv = yargs
             .options({
               address: {
                 demand : true,
                 describe : 'enter the address',
                 alias : 'a',
                 string : true
               }
             })
             .help()
             .argv;

  var encodedAddress = encodeURIComponent(argv.address);
  var encodedURL = `https://map.googleapis.com/maps/api/geocode/json?address=${encodedAddress}&key=AIzaSyBlryMpog7zpVh_LANQ1Q45FjCq1dhWEmI`;
  axios.get(encodedURL).then((response) => {
    if(response.data.status==='ZERO_RESULTS'){
      throw new Error('Unable to find that address');
    }
    console.log(`The Address is : ${response.data.results[0].formatted_address}`);
    var latitude = response.data.results[0].geometry.location.lat;
    var longitude = response.data.results[0].geometry.location.lng;
    var weatherURL = "https://api.darksky.net/forecast/9a6beebc5513206bc56851715f3a9a8e/"+latitude+","+longitude ;
    return axios.get(weatherURL);
  }).then((response) => {
      console.log(`the temperature is : ${response.data.currently.temperature}`);
    }).catch((e) => {
      if(e.message==='Request failed with status code 404'){
         console.log('Unable to connect to API servers');
       }
      else{
         console.log(e.message);
      }
  });
